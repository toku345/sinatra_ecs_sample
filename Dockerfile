FROM ruby:2.4.1-alpine

ENV APP_ENV=production

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY Gemfile /usr/src/app
COPY Gemfile.lock /usr/src/app
RUN bundle install

# Bundle app source
COPY app.rb /usr/src/app

EXPOSE 4567
CMD ["bundle", "exec", "ruby", "app.rb", "-p", "4567", "-b", "0.0.0.0"]
